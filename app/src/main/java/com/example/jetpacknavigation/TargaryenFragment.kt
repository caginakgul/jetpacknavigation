package com.example.jetpacknavigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_stark.*
import kotlinx.android.synthetic.main.fragment_targaryen.*

class TargaryenFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_targaryen, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        btnAegon.setOnClickListener {
            val action = TargaryenFragmentDirections.actionTargaryenFragmentToJonSnowFragment()
            action.whoami = getString(R.string.jon_snow_targaryen)
            view?.let { Navigation.findNavController(it).navigate(action) }
        }
    }
}